/**
* @description: 提示消息
* @param: String
* @returns: Promise
*/

export function slTip(title = '', type = 'none', duration=1000) {
	return new Promise((resolve, reject) => {
		// 显示对象
		let showObj = {
			title,
			duration,
			mask: true,
			icon: type,
			success() {
				setTimeout(() => {
					uni.hideToast();
					resolve()
				}, duration)
			}
		};
		uni.showToast(showObj);
	})
}

/**
* @description: loading
* @param: String
* @returns: Promise
*/
export function slLoading(title = '加载中...') {
	return new Promise((resolve, reject) => {
		// 显示对象
		uni.showLoading({
			title,
			mask: true
		})
		resolve()
	})
}
